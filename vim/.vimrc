" No need to press shift
let mapleader = ';'
" Enable full use of ; and :
nnoremap : ;
nnoremap ; :

" Escape easily
inoremap jk <Esc>
inoremap JK <Esc>

set nocompatible

set number            " line numbers on

set hlsearch          " heighlight searched term
set incsearch         " search incrementally
set ignorecase        " ignore pure lowercase in search
" remove highlight
nnoremap <leader>rm :nohlsearch

set hidden            " change buffers without the annoying save/undo

set backspace=indent,eol,start " enable bksp over everything

set tabstop=4         " tab = 4 spaces, not 8
set shiftwidth=4      " no soft tabs
set shiftround        " use multiple sft-width while < and >
set expandtab         " no more tabs

set autoindent        " indent on
set copyindent        " copy prev indent
set smarttab          " use shiftwidth not tabstop

set showmatch         " show matching parenthesis

set history=9999    " remember lots more history
set undolevels=999  " remember lots more commands

filetype plugin indent on " proper indent please
syntax on

set list            " list hidden chars
set listchars=tab:>.,trail:.,extends:#,nbsp:. " like these

fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun
autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()
