#! /usr/bin/env bash

dir=/etc/ssh
here=`pwd`/$(dirname "$0")

sudo rm $dir/sshd_config
sudo cp $here/sshd_config $dir/sshd_config
sudo chmod 644 $dir/sshd_config
sudo chown root:root $dir/sshd_config
