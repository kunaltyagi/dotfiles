#! /usr/bin/env bash

# add some color and formatting for my eyes
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

if [ "$(uname)" = "Darwin" ]; then
    alias ls='ls -G'
else
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias less='less -FRXd'                   # smarter less
alias more=less

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert

if [ "$(uname)" = "Darwin" ]; then
    # open files by default application
    alias ]='open'

    # osascript -e "display notification \"$(history | tail -n1 | TODO: remove the first numbers and gp of letters)\" with title \"$([ $? = 0 ] && echo 'Terminal' || echo 'Error')\" sound name \"default\""
else
    # open files by default application
    alias ]='xdg-open'

    alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
fi

if [ `which nvim | wc -l` -eq 1 ];
then
    alias vim='nvim'
fi

alias svim='sudoedit'

alias byobu-correct-display="/usr/local/Cellar/byobu/[0-9].[0-9]*/lib/byobu/include/tmux-detach-all-but-current-client"

# copy from cmdline
if [ "$(uname)" = "Darwin" ]; then
    alias copy="pbcopy"
else
    alias copy="xclip -selection clipboard"
fi
if `which flameshot`; then
    alias screenshot="flameshot gui"
else
    alias screenshot="notify-send -t 10000 'Flameshot not installed'"
fi

# python
alias py="python"

# easier rsync
alias sshync='rsync -azP --delete'
alias ssh_private='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'

# split window vertically
alias tmq='tmux split-window -d'
# example: tmq -l 8 htop
# split window horizontally
alias tmw='tmq -h'

alias docker_run='docker run --rm -it -v `pwd`":/storage:rw"'
#complete -o nospace -F _docker_run docker_run

[ `which thefuck 2>/dev/null` ] && eval "$(thefuck --alias oops)"

[ `which node 2>/dev/null` ] && alias nodejs='node'

if [ `which catkin 2>/dev/null` ]; then
    alias cb='catkin build'
    alias cb_this='cb --this'
    alias cb_alone='cb --no-deps'
    alias cb_all_alone='cb_this --no-deps'
fi

alias remove_pypath="export PYTHONPATH=''"

function ping()
{
    # Process args
    local i=0
    local options=""
    local host=""
    local file="$HOME/.ssh/config"

    # find the host asked by user
    for arg
    do
        i=$(($i+1))
        if [ "$i" -lt "$#" ]
        then
            options="${options} ${arg}"
        else
            host="${arg}"
        fi
    done

    # check if ssh config file exists
    if [ -e $file ]
    then
        # Find host
        local hostname=$(awk "\$1==\"Host\" {host=\$2} \$1==\"HostName\" && host==\"${host}\" {print \$2}" "$HOME/.ssh/config")
        if [ -z "$hostname" ]
        then
            hostname="$host"
        fi
    else
        hostname="$host"
    fi

    # Run ping
    /usr/bin/env ping $options $hostname
}

function pskill()
{
    process=`ps aux | grep $@ | awk '{print $2}'`
    echo "Killing process with ID:"
    echo "$process" | tee /dev/tty | xargs kill -9
}

function qwedfvbgfr_line()
{
    if [ $# -eq 3 ];
    then
        echo "\$ awk 'NR>=$2 && NR<=$3' $1"
        awk 'NR>=$2 && NR<=$3' $1
    else
        echo "Usage: \$ line filename min_line_no max_line_no"
    fi
}

function unrealbuild()
{
    local UNR_PATH=$HOME/catkin_ws/src/workspace/UnrealEngine;
    local RAND_NUM=$(( ( RANDOM % 1000) + 1000 ));
    local CURR_DIR=`pwd`;
    local PROJ_NAME=$(basename ${1%.uproject});

    ${UNR_PATH}/Engine/Binaries/DotNET/UnrealBuildTool.exe $PROJ_NAME -ModuleWithSuffix $PROJ_NAME $RAND_NUM Linux Development -editorrecompile -canskiplink "${CURR_DIR}/${PROJ_NAME}.uproject" -progress
}

function ros_rapyuta()
{
    . ~/virtualenv/noetic/bin/activate
    if (( $# == 1 )); then
        if [[ -f ~/rapyuta/"$1"/install/setup.bash ]]; then
            echo "Sourcing install"
            . ~/rapyuta/"$1"/install/setup.bash
        else
            echo "Sourcing devel"
            . ~/rapyuta/"$1"/devel/setup.bash
        fi
    else
        echo "Sourcing default"
        . ~/noetic_ws/install/setup.bash
    fi
}

function local_ros()
{
    export ROS_IP=127.0.0.1
    export ROS_MASTER_URI=http://$ROS_IP:11311
    echo "export ROS_IP=$ROS_IP"
    echo "export ROS_MASTER_URI=$ROS_MASTER_URI"
}
function remote_ros()
{
    if (( $# < 2)); then
        echo "Auto detecting IP, might be wrong"
        export ROS_IP=$(hostname -I | xargs -n 1 echo 2>/dev/null | head -n 1)
        if (( $# == 0 )); then
            echo "Setting ROS_MASTER to auto-detected IP"
            export ROS_MASTER_URI=http://$ROS_IP:11311
        else
            export ROS_MASTER_URI=http://$1:11311
        fi
    elif (( $# == 2)); then
        export ROS_IP=$1
        export ROS_MASTER_URI=http://$2:11311
    else
        echo "Don't know what to do with these arguments"
        return
    fi

    echo "export ROS_IP=$ROS_IP"
    echo "export ROS_MASTER_URI=$ROS_MASTER_URI"
}
