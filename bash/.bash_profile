#! /usr/bin/env bash

if [ ! $BASH_RC ]; then
    . ~/.bashrc
fi

function __exists_in()
{
    if [ $# -lt 2 ]; then
        echo 'Usage: $0 HAYSTACK NEEDLE <optional_grep_args>'
        echo 0
        return 1
    fi
    local var=$1
    shift
    local result=`echo $var | grep $@ | wc -l`
    echo $result
    return 0
}

function __prepend()
{
    if [ $# -lt 2 ]; then
        echo 'Usage: exists_in HAYSTACK NEEDLE <optional_grep_args>'
        return 1
    fi
    if [ `__exists_in $@` -eq 0 ]; then
        echo $2:$1
    else
        echo $1
    fi
    return 0
}

function __safe_prepend()
{
    if [ $# -eq 1 ]; then
        echo $1
    else
        __prepend $1 $2
    fi
    return 0
}

alias __is_installed='which 2>/dev/null'
alias on_gpu='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia'

if [ "$(uname)" = "Darwin" ]; then
    export PATH="/usr/local/bin:/opt/local/bin:$PATH"
    export PYTHONPATH="$PYTHONPATH:/usr/local/share/sip"
    export LC_ALL=en_US.UTF-8
    export LANG=en_US.UTF-8

    # for using gnu utils like they should be, without the g prefix
    PATH=`__safe_prepend $PATH /usr/local/opt/coreutils/libexec/gnubin`
    MANPATH=`__safe_prepend $MANPATH /usr/local/opt/coreutils/libexec/gnuman`
    PATH=`__safe_prepend $PATH /usr/local/opt/qt/bin`
    PKG_CONFIG_PATH=`__safe_prepend $PKG_CONFIG_PATH /usr/local/opt/qt/lib/pkgconfig`

    # coz Mac can't use OpenSSH
    export OPENSSL_ROOT_DIR=$(brew --prefix openssl)
    # PATH="/usr/local/opt/openssl/bin:$PATH"
    # Setting for make files
    # LDFLAGS:  -L/usr/local/opt/openssl/lib
    # CPPFLAGS: -I/usr/local/opt/openssl/include

    # For Python
    PATH="/usr/local/opt/python/libexec/bin:$PATH"
    PATH=`__safe_prepend $PATH $HOME/Library/Python/3.7/bin`

    # For Byobu
    export BYOBU_PREFIX=/usr/local
    _byobu_sourced=1 . /usr/local/Cellar/byobu/5.122/bin/byobu-launch 2>/dev/null || true
else
    echo "Nothing :)" > /dev/null
fi

export EDITOR='nvim'
export TERMINAL='konsole'

# for java
export JAVA_HOME="/usr/lib/jvm/default-java"

# for brew
if [ -f /home/linuxbrew/.linuxbrew/bin/brew ]; then
    eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
fi

# git flow
[ -f $HOME/.git-flow-completion.bash ] && . $HOME/.git-flow-completion.bash

# for rust
[ -f $HOME/.cargo/bin/rustc ] && export PATH=`__safe_prepend $PATH $HOME/.cargo/bin`

# wakatime
# [ -f $HOME/workspace/bash-wakatime/bash-wakatime.sh ] && source $HOME/workspace/bash-wakatime/bash-wakatime.sh

# CUDA
export CUDA_ROOT="/usr/local/cuda"
if [ -d "$CUDA_ROOT" ]; then
    export PATH=`__safe_prepend $PATH $CUDA_ROOT/bin`
    export LD_LIBRARY_PATH=`__safe_prepend $LD_LIBRARY_PATH $CUDA_ROOT/lib/`
    export CUDA_ROOT="$CUDA_ROOT/bin"
else
    unset CUDA_ROOT
fi

# haskell
[ `__is_installed ghc` ] && export PATH=`__safe_prepend $PATH $HOME/.cabal/bin`

if [ -d "$HOME/.nvm" ]; then
    export NVM_DIR="${XDG_CONFIG_HOME:-${HOME}/.config}/nvm"
    # Load nvm
    [ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh" 2> /dev/null
    # Load nvm bash_completion
    [ -s "$NVM_DIR/bash_completion" ] && source "$NVM_DIR/bash_completion"
fi

if [ -d '/usr/NX/bin' ]; then
    export PATH=`__safe_prepend $PATH /usr/NX/bin`
fi

if [ -d "$HOME/go/bin" ]; then
    export PATH=`__safe_prepend $PATH $HOME/go/bin`
fi

# set PATH so it includes user's private bin directories
if [ -d "$HOME/.local" ]; then
    export LD_LIBRARY_PATH=`__safe_prepend $LD_LIBRARY_PATH $HOME/.local/lib`
    export PKG_CONFIG_PATH=`__safe_prepend $PKG_CONFIG_PATH $HOME/.local/lib/pkgconfig`
    export CPATH="$HOME/.local/include"
    export PATH=`__safe_prepend $PATH $HOME/.local/bin`
fi

# keep this at the very end, we want the user's custom choices to be first
if [ -d "$HOME/local" ]; then
    export LD_LIBRARY_PATH=`__safe_prepend $LD_LIBRARY_PATH $HOME/local/lib`
    export CPATH="$HOME/local/include"
    export PATH=`__safe_prepend $PATH $HOME/local/bin`
fi

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

if [[ -d "$HOME/.rbenv" ]]; then
    export PATH=`__safe_prepend $PATH $HOME/.rbenv/bin`
    eval "$(rbenv init - bash)"
fi
