#! /usr/bin/env bash

# Install emacs only if X server is installed
if ! xset q &> /dev/null; then
    __check_and_install__ emacs ${apt_updated:-false}
else
    __check_and_install__ emacs ${apt_updated:-false} emacs-nox
fi
