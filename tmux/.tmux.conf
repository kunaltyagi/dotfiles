## To make neovim work properly
set -sg escape-time 0

## Key binding
# bind-key [-cnr] [-Tn] key
# -c: command mode
# -r: key can be repeated
# -T: it'll be specified if prefix or root is to be pressed before the key
# -n: short for "-T root". Root implies no requirement of prefix
# No -T or -n implies prefix is present

## Reload the config file
bind-key              r                 source-file ~/.tmux.conf

## Switch focus on panes
bind-key    -T root   C-h               select-pane -L
bind-key    -T root   C-l               select-pane -R
bind-key    -T root   C-k               select-pane -U
bind-key    -T root   C-j               select-pane -D

## For C-a command prefix
#unbind C-b
set-option -g prefix C-a

## Toggle sync
# a is prefix, so chosen
bind-key    -T prefix a                 setw synchronize-panes

## Vim keybindings for copy-mode
# another option is emacs
setw -g mode-keys vi
# for tmux 2.3 and lower only
set -g @shell_mode 'vi'

## Mouse integration
set -g mouse on

## No renaming windows
set-option -g allow-rename off

set-option -g history-limit 99999

## Helpful in remote sessions
# bind c new-window -c "#{pane_current_path}"

# load bash
set-option -g default-command "exec /usr/bin/env fish"

set -g @plugin 'tmux-plugins/tpm'
# set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-pain-control'
# set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-sidebar'
set -g @plugin 'tmux-plugins/tmux-yank'

# neovim sessions also
# set -g @resurrect-strategy-vim 'session'
# save pane-contents
# set -g @resurrect-capture-pane-contents 'on'

# start tmux server@boot and restore
# set -g @continuum-restore 'on'
# set -g @continuum-boot 'on'
# set -g @continuum-save-interval 15
# set -g status-right 'Continuum status: #{continuum_status}'

run '~/.tmux/plugins/tpm/tpm'
