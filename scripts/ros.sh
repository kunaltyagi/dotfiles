#! /usr/bin/env sh

# install gazebo
[ ! `which gazebo` ] && curl -sSL http://get.gazebosim.org | sh

gazebo_ver=`gazebo -v | sed -n 's/.*version[- ]*\([0-9.]*\).*/\1/pi' | head -n 1 | cut -d . -f 1`
ros=ros-${1:-kinetic}
echo "Installing for ${ros}"

# installation: all sudo
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'  &&
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116  &&
sudo apt update  &&
#if [ "$ros" = "ros-kinetic" ]; then
#    sudo apt install ros-build-tools
#fi &&
sudo apt install -y ${ros}-desktop &&
# These are installed because we want a custom simulator, not the pre-shipped gazebo
sudo apt install -y cmake ${ros}-perception ${ros}-urdf-tutorial ${ros}-catkin \
                    ${ros}-robot ${ros}-rqt ${ros}-controller-manager ${ros}-diff-drive-controller \
                    ${ros}-joint-state-controller ${ros}-position-controllers \
                    ${ros}-robot-state-publisher ${ros}-rqt-robot-steering ${ros}-rviz &&
# Now install ros and gazebo libraries
sudo apt install -y ${ros}-gazebo${gazebo_ver}-* &&

# install debug camera libs
sudo apt install ${ros}-video-stream-opencv &&
sudo apt install -y python-rosinstall python-rosinstall-generator python-wstool build-essential &&
sudo rosdep init &&

# configuration: all non-sudo
rosdep update

# update the rc files yourself
