#! /usr/bin/env bash

function sudo_installation() {
# ensure the system is already up to date
apt update;
apt install software-properties-common;  # for apt-add-repository

# ppa for latest
local ppa_list=(ubuntu-toolchain-r/test webupd8team/terminix gnome3-team/gnome3 \
          git-core/ppa rael-gc/rvm)
for ppa in "${ppa_list[@]}"
do
    if ! `grep -q "^deb .*${ppa}" /etc/apt/sources.list /etc/apt/sources.list.d/*.list`; then
        add-apt-repository -y ppa:${ppa};
    fi
done

wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
local sources_file=/etc/apt/sources.list.d/llvm.list
echo "deb http://apt.llvm.org/xenial/ llvm-toolchain-$(lsb_release -cs) main
# deb-src http://apt.llvm.org/xenial/ llvm-toolchain-$(lsb_release -cs) main" > ${sources_file}

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
sources_file=/etc/apt/sources.list.d/docker.list
echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable
# deb-src [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > ${sources_file}

echo "Update to get new sources, if any"
apt update;
apt upgrade -y;

# install the basics
apt install -y curl git mercurial mc p7zip-full openssh-server pv terminix \
               tmux tree vim wget xclip;
update-alternatives --config x-terminal-emulator;
# update-alternatives --config vim;

# gnome3
apt install gnome-shell ubuntu-gnome-desktop guake;

# install python
apt install -y python-pip  python3-pip python-dev python3-dev;

# install compile tools
apt install -y autoconf autoconf-archive automake autopoint autotools-dev \
               build-essential ccache cmake libtool pkg-config;
# cross-platform testing anyone
apt install -y gcc-arm-linux-gnueabihf binutils-arm-linux-gnueabi \
               libc6-dev-armhf-cross;
apt install -y qemu qemu-user-static qemu-user;
apt install -y docker-ce;
usermod -a -G docker $USER;

# install compilers
apt install -y clang gcc g++ llvm;
apt install -y flex flexc++ bison bisonc++;
update-alternatives --config c++;
update-alternatives --config cc;

# install developer tools: debuggers, etc.
apt install -y clang-tidy ctags gdb strace valgrind;
apt install -y valkyrie;  # front-end for valgrind

# install tools
apt install -y htop iftop iotop nmap wireshark;
usermod -a -G wireshark $USER;

# install filesystem support
apt install -y btrfs-tools gparted;

# vlc
apt install -y vlc vlc-nox;
apt install -y blender gimp inkspace;

# emacs
apt install -y emacs emacs-nox;

# install languages
# ruby
apt install -y rvm;
}

sudo bash -c "$(declare -f sudo_installation); sudo_installation"
snap install vlc;  # for 3.x
# for building cross-platform images
docker run --rm --privileged multiarch/qemu-user-static:register --reset;

ssh localhost /usr/share/rvm/bin/rvm install ruby;
# haskell
wget -qO- https://get.haskellstack.org/ | sh
stack install;
# rust
curl https://sh.rustup.rs -sSf | sh

# nvm
# curl -o- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

# yarn
# curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
# echo "deb https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
# sudo apt update
# sudo apt install --no-install-recommends yarn
