" For terminals where weird charachters appear, eg: terminator
" set guicursor=
" Never press shift ever again let mapleader = ';'
" Get the lost functionality back
nnoremap : ;
nnoremap ; :
let mapleader=":"

" Escape easily
inoremap jk <Esc>
inoremap JK <Esc>

" Move around more easily
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Goto end of search
nnoremap <silent>m //e<CR>

set nocompatible                        " when shall this disappear? :P

set number                              " line numbers on

set hlsearch                            " heighlight searched term
set incsearch                           " search incrementally
set ignorecase                          " ignore pure lowercase in search
" remove highlight
nnoremap <leader>rm :set invhlsearch

set hidden                              " have > 1 unsaved buffers

set backspace=indent,eol,start          " backspace over everything

set tabstop=4                           " tab = 4. Wanna use 8? Use hint line
set shiftwidth=4                        " no soft tabs
set shiftround                          " use multiple sft-width with <, >
set expandtab                           " no more tabs :) Use C-v + <Tab>

set autoindent                          " indent on
set copyindent                          " copy prev indent
set smarttab                            " use shiftwidth not tabstop

set list                                " list hidden chars
" show hidden chars specially
set listchars=tab:>.,trail:.,extends:#,nbsp:.
" remove trailing whitespaces. Don't use this with whitespace language
" Else append some visible character at the end of every line :)
" autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

set showmatch                           " show matching parenthesis

set wildmenu                            " cmdline completion

set history=9999                        " remember lots more history
set undolevels=999                      " remember lots more commands

" Present by default, so use it
" Jumpts to else from if, and other similar matches (press % in n mode)
runtime macros/matchit.vim

" Install vim-plug
" curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall -sync | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')

Plug 'tpope/vim-sensible'               " sensible defaults
Plug 'tpope/vim-scriptease'             " easy vimL
Plug 'tpope/vim-surround'               " good for XML, HTML
Plug 'tpope/vim-repeat'                 " repeat combination of commands
Plug 'tpope/vim-fugitive'               " git wrapper
Plug 'tpope/vim-commentary'             " better comments
" Plug 'tpope/vim-tbone'                  " integration with tmux
" Plug 'tpope/vim-obsession'              " save sessions
Plug 'tpope/vim-abolish'                " better substitutions
Plug 'arthurxavierx/vim-caser'          " visual mode case changes

" Plug 'easymotion/vim-easymotion'        " simple movements
" Plug 'justinmk/vim-sneak'               " smarter char mappings
" Plug 'kien/ctrlp.vim'                   " fuzzy finding

Plug 'Valloric/YouCompleteMe',          " Code completion
      \ { 'do': './install.py' }
Plug 'sheerun/vim-polyglot'             " language packs
Plug 'lervag/vimtex'                    " LaTeX specific
Plug 'docker/docker', { 'rtp': '/contrib/syntax/vim' }
Plug 'bfrg/vim-cpp-modern'
Plug 'luochen1990/rainbow'

Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

Plug 'tmux-plugins/vim-tmux'

Plug 'vim-airline/vim-airline'          " vim statusline
" Plug 'edkolev/tmuxline.vim'             " tmux statusline

Plug 'rhysd/vim-clang-format'           " For c, cpp
Plug 'psf/black', { 'tag': '*' }        " For python

Plug 'mattn/emmet-vim'                  " For html/css

Plug 'vimwiki/vimwiki'                  " For vimwiki

" Plug 'Shougo/denite.vim'                " interface uniter :)
" Plug 'Shougo/vimfiler.vim'              " netrw replacement
" Plug 'scrooloose/nerdtree'             " netrw replacement
" Plug 'tpope/vim-vinegar'                " netrw addon

Plug 'junegunn/vim-plug'                " self

Plug 'wakatime/vim-wakatime'            " wakatime for insights

call plug#end()
delc PlugUpgrade

" vimtex
let g:tex_flavor = 'plain'

" YCM
let g:ycm_global_ycm_extra_conf = '~/.local/share/nvim/plugged/YouCompleteMe/third_party/ycmd/examples/.ycm_extra_conf.py'
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_enable_diagnostic_highlighting = 0
let g:UltiSnipsExpandTrigger="<c-b>"

" clang-format
let g:clang_format#detect_style_file = 1
let g:clang_format#command = 'clang-format'

" autopep8
let g:autopep8_on_save = 1
let g:autopep8_disable_show_diff = 1
let g:autopep8_aggressive = 1

" cpp syntax
" Disable function highlighting (affects both C and C++ files)
: let g:cpp_no_function_highlight = 1
" Enable highlighting of named requirements (C++20 library concepts)
let g:cpp_named_requirements_highlight = 1

" rainbow parentheses
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle
let g:rainbow_conf = {
\   'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
\   'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
\   'operators': '_,_',
\   'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold'],
\   'separately': {
\       '*': {},
\       'tex': {
\           'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/'],
\       },
\       'lisp': {
\           'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick', 'darkorchid3'],
\       },
\       'vim': {
\           'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/', 'start=/{/ end=/}/ fold', 'start=/(/ end=/)/ containedin=vimFuncBody', 'start=/\[/ end=/\]/ containedin=vimFuncBody', 'start=/{/ end=/}/ fold containedin=vimFuncBody'],
\       },
\       'html': {
\           'parentheses': ['start=/\v\<((area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)[ >])@!\z([-_:a-zA-Z0-9]+)(\s+[-_:a-zA-Z0-9]+(\=("[^"]*"|'."'".'[^'."'".']*'."'".'|[^ '."'".'"><=`]*))?)*\>/ end=#</\z1># fold'],
\       },
\       'css': 0,
\   }
\}

" emmet-vim
let g:user_emmet_install_global = 0
let g:user_emmet_leader_key = '<C-C>'
augroup custom_filetype
  autocmd FileType html,css EmmetInstall
  autocmd FileType vim,html,css setlocal shiftwidth=2 tabstop=2
augroup end

" vimwiki
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

let @a = 'istatic_cast<std::size_t>(jk'
